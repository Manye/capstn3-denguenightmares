using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour
{
    [SerializeField] private Transform weaponViewport;
    [SerializeField] private Transform powerUpViewport;

    public int weaponCounter = 0;
    public int powerUpCounter = 0;

    public Sprite tempSprite;
    private void Awake()
    {
        SingletonManager.Register(this);
    }
    public void OnAddWeapon(WeaponStat weaponStat) 
    {
        
        WeaponInventoryContainer weaponInventory;
        if (checkForWeaponInViewport(weaponStat)) 
        {
            int weaponIndex = getWeaponIndex(weaponStat); // Gets the index of the weapon in inventory

            if (weaponIndex >= 0) // Checks if the weapon index is valid
            {
                weaponInventory = weaponViewport.GetChild(weaponIndex).GetComponent<WeaponInventoryContainer>();
                weaponInventory.SetWeaponStat(weaponStat);
                Debug.Log("Added wapoz");
            }
        }
        else
        {
            weaponInventory = weaponViewport.GetChild(weaponCounter).GetComponent<WeaponInventoryContainer>();
            weaponInventory.SetWeaponStat(weaponStat);
            weaponCounter++;
            Debug.Log("Added wapon");
        }
        
    }

    public void OnAddPowerUp(PowerupStat powerupStat) 
    {
        if (checkForPowerUpInViewport(powerupStat)) return;
        powerUpViewport.GetChild(powerUpCounter).GetComponent<PowerUpInventoryContainer>().SetPowerUpStat(powerupStat);
        powerUpCounter++;
    }

    private bool checkForWeaponInViewport(WeaponStat toAdd) 
    {
        for (int i = 0; i < weaponViewport.childCount; i++) 
        {
            WeaponInventoryContainer weaponInventoryContainer = weaponViewport.GetChild(i).gameObject.GetComponent<WeaponInventoryContainer>();
            if (weaponInventoryContainer.getWeaponName().Equals(toAdd.weaponName)) return true;
        }

        return false;
    }

    private bool checkForPowerUpInViewport(PowerupStat toAdd)
    {
        for (int i = 0; i < powerUpViewport.childCount; i++)
        {
            PowerUpInventoryContainer powerUpInventoryContainer = powerUpViewport.GetChild(i).gameObject.GetComponent<PowerUpInventoryContainer>();
            if (powerUpInventoryContainer.getPowerUpName().Equals(toAdd.powerupName)) return true;
        }

        return false;
    }

    private int getWeaponIndex(WeaponStat toAdd) 
    {
        for (int i = 0; i < weaponViewport.childCount; i++)
        {
            WeaponInventoryContainer weaponInventoryContainer = weaponViewport.GetChild(i).gameObject.GetComponent<WeaponInventoryContainer>();
            if (weaponInventoryContainer.getWeaponName().Equals(toAdd.weaponName)) return i;
        }

        return -1;
    }

    public bool powerUpFull() 
    {
        for (int i = 0; i < powerUpViewport.childCount; i++)
        {
            PowerUpInventoryContainer powerUpInventoryContainer = powerUpViewport.GetChild(i).gameObject.GetComponent<PowerUpInventoryContainer>();
            if (powerUpInventoryContainer.getPowerUpName().Equals("")) return false;
        }

        return true;
    }
}
