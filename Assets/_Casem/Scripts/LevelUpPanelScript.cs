using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpPanelScript : MonoBehaviour
{
    [Header(DS_Constants.ASSIGNABLE)]
    public GameObject rewardChoice; 
    public Transform parent;

    [Header(DS_Constants.DO_NOT_ASSIGN)]
    [SerializeField] private List<GameObject> spawnedChoices = new();
    private Button buttonToAddListener;

    private float scaleDuration = 0.1f;
    [SerializeField] private FactoidRandomizer factoidRandomizer;
    private void OnEnable()
    {
        this.transform.localScale = Vector3.zero;
        factoidRandomizer.SetRandomFactoid();
        StartCoroutine(ScaleCoroutine());

    }

    private IEnumerator ScaleCoroutine()
    {
        Vector3 initialScale = this.transform.localScale;
        Vector3 targetScale = Vector3.one; 

        float elapsedTime = 0f;

        while (elapsedTime < scaleDuration)
        {
            float progress = elapsedTime / scaleDuration; // Calculate the current progress of scaling (0 to 1)
            this.transform.localScale = Vector3.Lerp(initialScale, targetScale, progress); // Apply the scaling using Lerp function

            yield return null;
            elapsedTime += Time.deltaTime;  // Update the elapsed time
        }
        this.transform.localScale = targetScale;  // Ensure the object reaches full scale (in case of any rounding issues)
        SingletonManager.Get<GameManager>().PauseGameTime(true);
    }

    public void SpawnPowerUpChoice(PowerupStat powerUpStat) 
    {
        GameObject toSpawn = (GameObject)Instantiate(rewardChoice, parent);
        SelectionChoice selectionChoice = toSpawn.GetComponent<SelectionChoice>();
        selectionChoice.InitializePowerUpData(powerUpStat);
        buttonToAddListener = selectionChoice.getButton();
        spawnedChoices.Add(toSpawn);
        toSpawn.SetActive(true);
    }
    public void SpawnWeaponChoice(WeaponStat weaponStat)
    {
        GameObject toSpawn = (GameObject)Instantiate(rewardChoice, parent);
        SelectionChoice selectionChoice = toSpawn.GetComponent<SelectionChoice>();
        selectionChoice.InitializeWeaponData(weaponStat);
        buttonToAddListener = selectionChoice.getButton();
        spawnedChoices.Add(toSpawn);
        toSpawn.SetActive(true);
    }

    public void SpawnPointsChoice()
    {

        GameObject toSpawn = (GameObject)Instantiate(rewardChoice, parent);
        SelectionChoice selectionChoice = toSpawn.GetComponent<SelectionChoice>();
        selectionChoice.InitializePointChoice();
        buttonToAddListener = selectionChoice.getButton();
        spawnedChoices.Add(toSpawn);
        toSpawn.SetActive(true);
    }
    public void TruncateChoices() 
    {
        foreach (GameObject g in spawnedChoices) Destroy(g);
        spawnedChoices.Clear();
    }

    public int getSpawnChoicesCount() => spawnedChoices.Count;
    public Button getCurrentButton() => buttonToAddListener;

    //Needs Cleanup
    public bool CheckDuplicates(WeaponStat weaponStat, PowerupStat powerUpStat)
    {
        bool hasDuplicate = false;
        foreach (GameObject choice in spawnedChoices) 
        {
            SelectionChoice selectionChoice = choice.GetComponent<SelectionChoice>();

            if (weaponStat != null)
            {
                if (weaponStat.rewardSO.rewardName.Equals(selectionChoice.getName()))
                {
                    hasDuplicate = true;
                    break;
                }
            }
            else 
            {
                if (powerUpStat.rewardSO.rewardName.Equals(selectionChoice.getName()))
                {
                    hasDuplicate = true;
                    break;
                }
            }
        }
        return hasDuplicate;
    }
}
