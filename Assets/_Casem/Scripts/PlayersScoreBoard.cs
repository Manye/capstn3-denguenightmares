using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine.UI;
#region Serializable Classes

[System.Serializable]
public class PlayerScore
{
    public string playerName;
    public int score;

    public PlayerScore(string playerName, int score)
    {
        this.playerName = playerName;
        this.score = score;
    }
}

[System.Serializable]
public class PlayerScoreBoard
{
    public List<PlayerScore> scores = new List<PlayerScore>();

    public void AddScore(string playerName, int score)
    {
        PlayerScore newScore = new PlayerScore(playerName, score);
        scores.Add(newScore);
    }
    public void SortScoresByScoreDescending()
    {
        scores = scores.OrderByDescending(score => score.score).ToList();
    }
}
#endregion
public class PlayersScoreBoard : MonoBehaviour
{
    [Header("Scoreboard Data")]
    [SerializeField] private PlayerScoreBoard playerScoreBoard;
    private const string fileName = "playerScoreBoard.json";
    private string filePath;

    [Header("Testing Parameters")]
    public string playerName;
    public int playerScore;

    [SerializeField] private HighscoreScoreboard highScoreBoard;

    [Header("User Interfaces")]
    [SerializeField] private GameObject highScoreInputPrompt;
    [SerializeField] private GameObject noHighScorePrompt;
    [SerializeField] private GameObject highScoreList;
    [SerializeField] private GameObject highScoreBoardGO;

    [Header("User Interface Outside Scoreboard")]
    [SerializeField] private Button retryButton;
    [SerializeField] private Button menuButton;

    private void Awake()
    {
        filePath = Path.Combine(Application.dataPath, fileName);
    }
    private void Start() => LoadPlayerScoreBoard();
    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.L)) OpenHighScoreBoard();
        if (Input.GetKeyDown(KeyCode.K)) ClearHighScore();
        if (Input.GetKeyDown(KeyCode.H)) AddScore(playerName, playerScore);
#endif
    }
    #region Callable Commands
    // Add a new score to the player score board
    public void AddScore(string playerName, int score)
    {
        playerScoreBoard.AddScore(playerName, score);
        SavePlayerScoreBoard();
        LoadPlayerScoreBoard();
        OpenHighScoreBoard();
    }
    public void SavePlayerScore(TMP_InputField nameInputField) // Called from Scoreboard Entry GameObject
    {
        int retrievedScore = SingletonManager.Get<GameManager>().playerPoints;
        playerScoreBoard.AddScore(nameInputField.text, retrievedScore);
        SavePlayerScoreBoard();

        OpenHighScoreBoard();
    }
    public void CheckForHighScore() // Being called right after the player wins the game
    {
        int retrievedScore = SingletonManager.Get<GameManager>().playerPoints;

        if (playerScoreBoard.scores.Count < 10)
        {
            if (!IsThereEqualScore(retrievedScore))
            {
                highScoreInputPrompt.SetActive(true);
            }
        }
        else if (playerScoreBoard.scores.Count >= 10)
        {
            if (retrievedScore > getLastHighScore() && !IsThereEqualScore(retrievedScore))
            {
                highScoreInputPrompt.SetActive(true); // Enables HighScore Input
            }
        }
        //else open canvas that displays player score and menu navigation
    }
    private int getLastHighScore()
    {
        int lastScore;
        if (playerScoreBoard.scores.Count > 0)
        {
            int lastIndex = playerScoreBoard.scores.Count - 1;
            lastScore = playerScoreBoard.scores[lastIndex].score;
        }
        else lastScore = 0;

        return lastScore;
    }

    private bool IsThereEqualScore(int retrievedScore)
    {
        for (int i = 0; i < playerScoreBoard.scores.Count; i++)
        {
            if (retrievedScore.Equals(playerScoreBoard.scores[i].score))
            {
                return true;
            }
        }
        return false;
    }

    #endregion
    #region UI Commands
    private void SetupHighScoreBoard(PlayerScoreBoard playerScoreBoard)
    {
        if (highScoreBoard) highScoreBoard.InitializeHighScoreBoard(playerScoreBoard.scores);
        else Debug.Log("Highscore Board Not Found");
    }

    public void CloseHighScoreBoard() => highScoreBoardGO.SetActive(false);
    private void SetPromptsDisable()
    {
        if (highScoreList.activeSelf) highScoreList.SetActive(false);
        if (noHighScorePrompt.activeSelf) noHighScorePrompt.SetActive(false);
    }
    public void OpenHighScoreBoard()
    {
        SetPromptsDisable();

        highScoreBoardGO.SetActive(true);

        if (playerScoreBoard.scores.Count > 0)
        {
            highScoreList.SetActive(true);
            SetupHighScoreBoard(playerScoreBoard);
        }
        else noHighScorePrompt.SetActive(true);

    }
    private void ClearHighScore()
    {
        playerScoreBoard = new();
        SavePlayerScoreBoard();
        SetupHighScoreBoard(playerScoreBoard);
        CloseHighScoreBoard();
        OpenHighScoreBoard();
    }
    #endregion
    #region JSON Fetching
    // Save player score board to JSON file
    public void SavePlayerScoreBoard()
    {
        playerScoreBoard.SortScoresByScoreDescending();
        string json = JsonUtility.ToJson(playerScoreBoard);
        File.WriteAllText(filePath, json);
        Debug.Log("Player score board saved to: " + filePath);
    }

    // Load player score board from JSON file
    public void LoadPlayerScoreBoard()
    {
        if (File.Exists(filePath))
        {
            string json = File.ReadAllText(filePath);
            playerScoreBoard = JsonUtility.FromJson<PlayerScoreBoard>(json);
        }
        else Debug.LogWarning("Player score board file not found.");
    }
    #endregion
}
