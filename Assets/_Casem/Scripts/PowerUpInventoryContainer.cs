using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpInventoryContainer : MonoBehaviour
{
    [SerializeField] private PowerupStat powerupStat;

    [SerializeField] private Sprite sprite;
    [SerializeField] private Image image;

    public void OnPowerUpSet()
    {
        image.sprite = powerupStat.rewardSO.sprite;
    }

    public void SetPowerUpStat(PowerupStat powerupStat)
    {
        this.powerupStat = powerupStat;

        OnPowerUpSet();
    }

    public string getPowerUpName() => powerupStat.powerupName;
}
