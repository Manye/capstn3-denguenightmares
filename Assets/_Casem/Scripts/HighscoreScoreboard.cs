using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighscoreScoreboard : MonoBehaviour
{
    public List<GameObject> highScoreUIContainers;
    public GameObject highScoreContainerPrefab;
    public Transform highScoreViewPort;
    public void TruncateHighScore() 
    {
        foreach (GameObject highScoreUI in highScoreUIContainers) 
        {
            Destroy(highScoreUI);
        }

        highScoreUIContainers.Clear();
    }

    public void InitializeHighScoreBoard(List<PlayerScore> playerScores) 
    {
        TruncateHighScore();
        int counter = 0;
        foreach (PlayerScore playerScore in playerScores) 
        {
            if (highScoreUIContainers.Count >= 10) break;

            GameObject cache = (GameObject)Instantiate(highScoreContainerPrefab, highScoreViewPort);
            cache.GetComponent<HighscoreUIContainer>().InitializeData(playerScore,counter + 1);

            cache.SetActive(true);
            highScoreUIContainers.Add(cache);

            counter++;
        }
    }
}
