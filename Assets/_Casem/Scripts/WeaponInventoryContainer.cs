using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponInventoryContainer : MonoBehaviour
{
    [SerializeField] private WeaponStat weaponStat;

    [SerializeField] private Sprite sprite;
    [SerializeField] private Image image;

    public void OnWeaponSet() 
    {
        if(!weaponStat.isEvolved) image.sprite = weaponStat.rewardSO.sprite;
        else image.sprite = weaponStat.rewardSO.evolvedSprite;

    }

    public void SetWeaponStat(WeaponStat weaponStat) 
    {
        this.weaponStat = weaponStat;

        OnWeaponSet();
    }

    public WeaponStat getWeaponStat() => weaponStat;
    public string getWeaponName() => weaponStat.weaponName;
}
