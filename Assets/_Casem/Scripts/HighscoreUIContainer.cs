using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class HighscoreUIContainer : MonoBehaviour
{
    public TextMeshProUGUI playerName;

    public TextMeshProUGUI playerScore;


    public void InitializeData(PlayerScore playerScore,int index) 
    {
        playerName.text = "0" + index.ToString() + "     " + playerScore.playerName;
        this.playerScore.text = playerScore.score.ToString();

    }
}
