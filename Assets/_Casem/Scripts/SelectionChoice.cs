using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class SelectionChoice : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI rewardNameLabel;
    [SerializeField] private Image rewardImage;
    [SerializeField] private TextMeshProUGUI rewardDescription;
    [SerializeField] private TextMeshProUGUI statusLabel;

    [SerializeField] private Button selectionButton;
    [SerializeField] private Sprite pointsSprite;

    public bool isWeapon; // Quick Fix
    public void InitializeWeaponData(WeaponStat weaponStat)
    {
        rewardNameLabel.text = weaponStat.rewardSO.rewardName;
        // Uncomment this when sprites are up and running
        Sprite sprite = weaponStat.level == 5 ? weaponStat.rewardSO.evolvedSprite : weaponStat.rewardSO.sprite;
        rewardImage.sprite = sprite;
        rewardDescription.text = weaponStat.rewardSO.rewardDescriptions[weaponStat.level];
        // statusLabel.text = weaponStat.weaponSM.isActiveAndEnabled ? "" : "New!";
        if (weaponStat.level >= 1 && weaponStat.level < 5 )
        {
            statusLabel.text = $"Lvl {weaponStat.level + 1}";
            statusLabel.color = SingletonManager.Get<UIManager>().colorLevelUp;
        }
        else if (weaponStat.level >= 5)
        {
            statusLabel.text = "Evolve!";
            statusLabel.color = SingletonManager.Get<UIManager>().colorEvolved;
        }
        else
        {
            statusLabel.text = "New!";
            statusLabel.color = SingletonManager.Get<UIManager>().colorNew;
        }
        isWeapon = true;
    }

    public void InitializePowerUpData(PowerupStat powerUpStat)
    {
        rewardNameLabel.text = powerUpStat.rewardSO.rewardName;
        // Uncomment this when sprites are up and running
        rewardImage.sprite = powerUpStat.rewardSO.sprite;
        rewardDescription.text = powerUpStat.rewardSO.rewardDescriptions[powerUpStat.level + 1];
        if (powerUpStat.level > 0)
        {
            statusLabel.text = $"Lvl {powerUpStat.level + 1}";
            statusLabel.color = SingletonManager.Get<UIManager>().colorLevelUp;
        }
        else
        {
            statusLabel.text = "New!";
            statusLabel.color = SingletonManager.Get<UIManager>().colorNew;
        } 
        isWeapon = false;
    }

    public void InitializePointChoice() 
    {
        rewardNameLabel.text = "Points";
        // Uncomment this when sprites are up and running
        rewardImage.sprite = pointsSprite;
        rewardDescription.text = "Extra Points!";
        statusLabel.text = "BONUS";
    }
    public Button getButton() => selectionButton; //selectionButton.addListener
    public string getName() => rewardNameLabel.text;

}
