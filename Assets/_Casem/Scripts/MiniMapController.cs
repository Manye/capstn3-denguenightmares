using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapController : MonoBehaviour
{
    public GameObject miniMap;
    public GameObject miniMapBorderImg;
    public KeyCode miniMapToggle;
    public KeyCode miniMapToggle2;

    private void Update()
    {
        if (Input.GetKeyDown(miniMapToggle) || Input.GetKeyDown(miniMapToggle2))
        {
            miniMap.SetActive(!miniMap.activeSelf);
            miniMapBorderImg.SetActive(!miniMapBorderImg.activeSelf);
        }
    }
}
