using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeOutImage : MonoBehaviour
{
    public Image imageToFade;
    public float fadeDuration = 1.0f;

    public void Start()
    {
        StartCoroutine(FadeOutCoroutine());
    }

    private IEnumerator FadeOutCoroutine()
    {
        float startAlpha = imageToFade.color.a;
        float targetAlpha = 0f;
        float elapsedTime = 0f;
        Color32 startColor32 = imageToFade.color;

        while (elapsedTime < fadeDuration)
        {
            float newAlpha = Mathf.Lerp(startAlpha, targetAlpha, elapsedTime / fadeDuration);
            Color32 newColor32 = startColor32;
            newColor32.a = (byte)(newAlpha * 255);
            imageToFade.color = newColor32;
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        Color32 finalColor32 = startColor32;
        finalColor32.a = 0;
        imageToFade.color = finalColor32;
        this.gameObject.SetActive(false);
    }
}
