using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SimpleFlash : MonoBehaviour
{
    [SerializeField] private Material playerFlashMaterial;
    private SpriteRenderer spriteRenderer;
    private Coroutine flashCoroutine;
    private Material originalMat;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalMat = spriteRenderer.material;
    }
    
    private void OnEnable()
    {
        spriteRenderer.material = originalMat;
    }
    
    public void Flash(float amount, float multiplier)
    {
        /*if (flashCoroutine != null)
        {
            StopAllCoroutines();
        }*/
        if (gameObject.activeInHierarchy)
        {
            flashCoroutine ??= StartCoroutine(FlashCoroutine(amount, multiplier));
        }
    }

    private IEnumerator FlashCoroutine(float amount, float multiplier)
    {
        spriteRenderer.material = playerFlashMaterial ? playerFlashMaterial : SingletonManager.Get<UIManager>().whiteFlashMat;
        yield return new WaitForSeconds(SingletonManager.Get<UIManager>().flashDuration); 
        transform.parent.GetComponent<Stat>().unitHealth.Damage(amount, multiplier);
        spriteRenderer.material = originalMat;
        flashCoroutine = null;
    }
}
