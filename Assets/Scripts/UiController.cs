using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Video;
public class UiController : MonoBehaviour
{
    private AudioManager audioMgr;

    [Header ("Settings Screen Sliders")]
    public Slider settingsMusicSlider;
    public Slider settingsSfxSlider;

    public VideoPlayer videoPlayer;
    void Start()
    {
        audioMgr = SingletonManager.Get<AudioManager>();

        // initialize the values of the sliders to 0.7
        // audioMgr.AdjustMusicVolume(0.7f);
        // audioMgr.AdjustSFXVolume(0.7f);

    }

    public void ToggleMusic()
    {
        audioMgr.ToggleMusic();
    }

    public void ToggleSFX()
    {
        audioMgr.ToggleSFX();
    }

    public void OnButtonClick()
    {
        audioMgr.PlaySFX("Button Click");
    }

    public void OnSFXSliderReleased()
    {
        // insert a good sfx for this part
        audioMgr.PlaySFX("Button Click 3");
    }

    public void OnBackButtonClicked()
    {
        audioMgr.PlaySFX("Button Click");
    }

    public void OnEnterButtonClicked()
    {
        audioMgr.PlaySFX("Button Click 2");
    }

    public void OnEnterPlayButtonClicked()
    {
        audioMgr.PlaySFX("Button Click 3");
    }

    public void AdjustMusicVolume()
    {
        videoPlayer.SetDirectAudioVolume(0, settingsMusicSlider.value);
    }


}
