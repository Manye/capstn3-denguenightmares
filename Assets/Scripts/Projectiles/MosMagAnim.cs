using UnityEngine;

public class MosMagAnim : MonoBehaviour
{
    [Header(DS_Constants.ASSIGNABLE)]
    public MosMagnet mosMagnet;
    public CircleCollider2D mosMagCC2D;
    public GameObject mosMagRange;
    public GameObject magnetParent;
    
    public void OnActivate()
    {
        mosMagnet.animator.SetTrigger("Online");
        mosMagnet.StartDamageCo();
        mosMagnet.StartDestructCo();
        mosMagCC2D.enabled = true;
        mosMagRange.SetActive(true);
    }

    public void OnDeactivate()
    {
        magnetParent.SetActive(false);
    }
}
