using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MosMagRange : Projectile
{
    private List<EnemyMovement> enemyMovements = new();

    protected override void OnEnable()
    {
        UpdateProjectileStats();
        if (isEvolved)
        {
            // Set new animation controller
            animator.runtimeAnimatorController = evolvedController;
        }
        animator.SetTrigger("Play");
    }

    private void OnDisable()
    {
        base.OnDisable();
        SingletonManager.Get<GameManager>().onChangeTargetEvent.Invoke(null);
        for (int i = 0; i < enemyMovements.Count; i++)
        {
            var em = enemyMovements[i];
            em.RemoveListenerToChangeTarget();
        }
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent(out EnemyMovement enemyMovement))
        {
            enemyMovements.Add(enemyMovement);
            enemyMovement.AddListenerToChangeTarget();
            SingletonManager.Get<GameManager>().onChangeTargetEvent.Invoke(transform.parent);
        }
    }

    protected override void UpdateProjectileStats()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                // Evolved?
                isEvolved = ws.isEvolved;
            }
        }
    }
}
