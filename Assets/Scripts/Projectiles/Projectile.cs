using System.Collections;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header(DS_Constants.ASSIGNABLE)] 
    protected bool isEvolved;
    public Animator animator;
    [SerializeField] protected RuntimeAnimatorController evolvedController;
    [SerializeField] protected bool isDestruct;
    [SerializeField] protected bool isMoving;
    protected float projectileSpeed;
    protected Vector2 projectileSize;
    protected float projectileDamage;
    protected float damageTimeTick;
    [SerializeField] protected SO_Pool poolSO; 
    protected float destructTimer;
    
    protected virtual void OnEnable()
    {
        UpdateProjectileStats();
        StartCoroutine((SelfDestructTimer()));
        
        // isEvolved?
        if (isEvolved)
        {
            // Set new animation controller
            animator.runtimeAnimatorController = evolvedController;
        }
        animator.SetTrigger("Play");
    }

    protected virtual void OnDisable()
    {
        UpdateProjectileStats();
    }

    protected virtual void Start()
    {
        UpdateProjectileStats();
    }

    protected virtual void Update()
    {
        if (isMoving)
        {
            transform.Translate(0, projectileSpeed * Time.deltaTime, 0);
        }        
    }

    protected virtual IEnumerator SelfDestructTimer()
    {
        yield return new WaitForSeconds(destructTimer);
        //Debug.Log("Self Destruct Projectile.");
        gameObject.SetActive(false);
    }

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log(col);
        if (col.gameObject.TryGetComponent(out EnemyStat enemyStat))
        {
            enemyStat.TakeDamage(projectileDamage, 0);
            // Debug.Log("Enemy took " + projectileDamage + " damage.");
        }
    }
    
    protected virtual void UpdateProjectileStats()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                // Evolved?
                isEvolved = ws.isEvolved;
                
                // Scale
                transform.localScale.Set(
                    ws.weaponLevelSO.defaultSize.x, 
                    ws.weaponLevelSO.defaultSize.y, 
                    ws.weaponLevelSO.defaultSize.z);
                var cacheX = ws.weaponLevelSO.defaultSize.x;
                var cacheY = ws.weaponLevelSO.defaultSize.y;
                cacheX *= ws.sizeIncrease;
                cacheY *= ws.sizeIncrease;
                transform.localScale.Set(
                    ws.weaponLevelSO.defaultSize.x + cacheX,
                    ws.weaponLevelSO.defaultSize.y + cacheY, 
                    ws.weaponLevelSO.defaultSize.z);
                
                // Damage
                projectileDamage = ws.weaponLevelSO.defaultDamage;
                var cacheDamage = projectileDamage * ws.damageIncrease;
                projectileDamage += cacheDamage;
                
                // Speed
                projectileSpeed = ws.weaponLevelSO.defaultSpeed;
                var cacheSpeed = projectileSpeed * ws.speedIncrease;
                projectileSpeed += cacheSpeed;
                
                // Damage Time Tick
                damageTimeTick = ws.weaponLevelSO.defaultDamageTick;
                var cacheDamageTick = damageTimeTick * ws.damageTickIncrease;
                damageTimeTick -= cacheDamageTick;
                
                // Destruct Timer
                destructTimer = ws.weaponLevelSO.defaultDestructTimer;
                var cacheDestructTimer = destructTimer * ws.destructTimerIncrease;
                destructTimer += cacheDestructTimer;
            }
        }
        foreach (PowerupStat ps in SingletonManager.Get<PowerupsManager>().powerups)
        {
            switch (ps.powerupName)
            {
                case "Radius":
                {
                    Vector3 thisScale = transform.localScale;
                    var cacheX = thisScale.x * ps.increase;
                    var cacheY = thisScale.y * ps.increase;
                    thisScale.Set(
                        thisScale.x + cacheX,
                        thisScale.y + cacheY,
                        thisScale.z);
                    break;
                }
                case "Damage":
                    var cacheDamage = projectileDamage * ps.increase;
                    projectileDamage += cacheDamage;
                    break;
                case "Speed":
                    var cacheSpeed = projectileSpeed * ps.increase;
                    projectileSpeed += cacheSpeed;
                    break;
                case "Time":
                    var cacheDestructTimer = destructTimer * ps.increase;
                    destructTimer += cacheDestructTimer;
                    break;
            }
        }
    }
}
