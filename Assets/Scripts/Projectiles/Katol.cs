using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Katol : Projectile
{
    public List<EnemyStat> enemyStatList = new();
    
    protected override void OnEnable()
    {
        UpdateProjectileStats();
        if (isDestruct)
        {
            base.OnEnable();
        }
        StartCoroutine(DamageAll());
        animator.SetTrigger("Play");
    }

    protected override void Start()
    {
        Debug.Log(projectileDamage);
        SingletonManager.Get<GameManager>().onUpdateUpgradesEvent.AddListener(UpdateProjectileStats);
        Debug.Log(projectileDamage);
    }

    protected override void OnDisable()
    {
        UpdateProjectileStats();
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out EnemyStat enemyStat))
        {
            enemyStatList.Add(enemyStat);
        }
    }

    protected void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out EnemyStat enemyStat))
        {
            /*enemyStat.StopDoT();
            enemyStat.ResetMoveSpeed();*/
            if (enemyStatList.Contains(enemyStat))
            {
                enemyStat.ResetMoveSpeed();
                enemyStatList.Remove(enemyStat);
            }
        }
    }

    private IEnumerator DamageAll()
    {
        while (true)
        {
            for (var index = 0; index < enemyStatList.Count; index++)
            {
                var es = enemyStatList[index];
                if (es.isActiveAndEnabled)
                {
                    es.TakeDamage(projectileDamage, 0);
                    if (isEvolved) es.DecrementMoveSpeed(0.9f);
                    else es.DecrementMoveSpeed(es.enemyStatSO.katolSlowPercent);
                    if (!es.isActiveAndEnabled)
                    {
                        enemyStatList.Remove(es);
                    }
                }
            }
            yield return new WaitForSeconds(damageTimeTick);
            if (enemyStatList.Count > 0)
            {
                SingletonManager.Get<AudioManager>().PlaySFX("Enemy Damage");
            }
        }
    }

    protected override void UpdateProjectileStats()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                isEvolved = ws.isEvolved;
                
                // Scale
                transform.localScale = new Vector3(ws.weaponLevelSO.defaultSize.x, ws.weaponLevelSO.defaultSize.y, 0);
                var lScale = transform.localScale;
                lScale.x *=  ws.sizeIncrease;
                lScale.y *= ws.sizeIncrease;
                transform.localScale += lScale;

                // Damage
                projectileDamage = ws.weaponLevelSO.defaultDamage;
                var cacheDamage = projectileDamage * ws.damageIncrease;
                projectileDamage += cacheDamage;

                // Damage Time Tick
                damageTimeTick = ws.weaponLevelSO.defaultDamageTick;
                var cacheDamageTick = damageTimeTick * ws.damageTickIncrease;
                damageTimeTick -= cacheDamageTick;
                
                // Destruct Timer
                destructTimer = ws.weaponLevelSO.defaultDestructTimer;
                var cacheDestructTimer = destructTimer * ws.destructTimerIncrease;
                destructTimer += cacheDestructTimer;
            }
        }
        foreach (PowerupStat ps in SingletonManager.Get<PowerupsManager>().powerups)
        {
            switch (ps.powerupName)
            {
                case "Radius":
                {
                    var lScale = transform.localScale;
                    lScale.x *=  ps.increase;
                    lScale.y *= ps.increase;
                    transform.localScale += lScale;
                    break;
                }
                case "Damage":
                    var cacheDamage = projectileDamage * ps.increase;
                    projectileDamage += cacheDamage;
                    break;
                case "AtkRate":
                    var cacheDamageTimeTick = damageTimeTick * ps.increase;
                    damageTimeTick -= cacheDamageTimeTick;
                    break;
                case "Time":
                    var cacheDestructTimer = destructTimer * ps.increase;
                    destructTimer += cacheDestructTimer;
                    break;
            }
        }
        
        if (isEvolved)
        {
            // Set new animation controller
            animator.runtimeAnimatorController = evolvedController;
        }
    }
}