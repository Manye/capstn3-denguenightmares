using UnityEngine;

public class ZapperAnim : MonoBehaviour
{
    [Header(DS_Constants.ASSIGNABLE)]
    public Zapper zapper;
    public CircleCollider2D zapperCC2D;
    public GameObject zapperRange;
    public GameObject zapperParent;

    public void OnActivate()
    {
        zapper.animator.SetTrigger("Online");
        zapper.StartDamageCo();
        zapper.StartDestructCo();
        zapperCC2D.enabled = true;
        zapperRange.SetActive(true);
    }

    public void OnDeactivate()
    {
        zapperParent.SetActive(false);
    }
}
