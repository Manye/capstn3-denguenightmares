using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZapperRange : Projectile
{
    protected override void OnEnable()
    {
        UpdateProjectileStats();
        if (isEvolved)
        {
            // Set new animation controller
            animator.runtimeAnimatorController = evolvedController;
        }
        animator.SetTrigger("Play");
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        
    }

    protected override void UpdateProjectileStats()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                // Evolved?
                isEvolved = ws.isEvolved;
            }
        }
    }
}
