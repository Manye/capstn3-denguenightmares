using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Insecticide : Projectile
{
    private List<EnemyStat> enemyStatList = new();
    protected override void OnEnable()
    {
        if (isDestruct)
        {
            base.OnEnable();
        }
        if (GetComponent<CircleCollider2D>()) GetComponent<CircleCollider2D>().enabled = true;
        StartCoroutine(DamageAll());
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        for (var index = 0; index < enemyStatList.Count; index++)
        {
            enemyStatList[index].StopDoT();
        }
        enemyStatList.Clear();
        StopAllCoroutines();
    }

    protected override IEnumerator SelfDestructTimer()
    {
        yield return new WaitForSeconds(destructTimer);
        PlayOfflineAnim();
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out EnemyStat enemyStat))
        {
            enemyStatList.Add(enemyStat);
        }
    }

    protected void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out EnemyStat enemyStat))
        {
            //enemyStat.StopDoT();
            if (enemyStatList.Contains(enemyStat))
            {
                enemyStatList.Remove(enemyStat);
            }
        }
    }

    private IEnumerator DamageAll()
    {
        while (true)
        {
            for (var index = 0; index < enemyStatList.Count; index++)
            {
                var es = enemyStatList[index];
                if (es.isActiveAndEnabled)
                {
                    es.TakeDamage(projectileDamage, es.insecticideDefPercent);
                    if (!es.isActiveAndEnabled)
                    {
                        enemyStatList.Remove(es);
                    }
                }
            }
            if (enemyStatList.Count > 0)
            {
                SingletonManager.Get<AudioManager>().PlaySFX("Enemy Damage");
            }
            yield return new WaitForSeconds(damageTimeTick);
        }
    }
    
    private void PlayOfflineAnim()
    {
        animator.SetTrigger("Offline");
        if (GetComponent<CircleCollider2D>()) GetComponent<CircleCollider2D>().enabled = false;
    }

    public void OnDeactivate()
    {
        gameObject.SetActive(false);
    }
    
    protected override void UpdateProjectileStats()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                isEvolved = ws.isEvolved;

                // Scale
                transform.localScale = new Vector3(ws.weaponLevelSO.defaultSize.x, ws.weaponLevelSO.defaultSize.y, 0);
                var lScale = transform.localScale;
                lScale.x *=  ws.sizeIncrease;
                lScale.y *= ws.sizeIncrease;
                transform.localScale += lScale;
                
                // Damage
                projectileDamage = ws.weaponLevelSO.defaultDamage;
                var cacheDamage = projectileDamage * ws.damageIncrease;
                projectileDamage += cacheDamage;

                // Speed
                projectileSpeed = ws.weaponLevelSO.defaultSpeed;
                var cacheSpeed = projectileSpeed * ws.speedIncrease;
                projectileSpeed += cacheSpeed;

                // Damage Time Tick
                damageTimeTick = ws.weaponLevelSO.defaultDamageTick;
                var cacheDamageTick = damageTimeTick * ws.damageTickIncrease;
                damageTimeTick -= cacheDamageTick;

                // Destruct Timer
                destructTimer = ws.weaponLevelSO.defaultDestructTimer;
                var cacheDestructTimer = destructTimer * ws.destructTimerIncrease;
                destructTimer += cacheDestructTimer;
            }
        }
        foreach (PowerupStat ps in SingletonManager.Get<PowerupsManager>().powerups)
        {
            switch (ps.powerupName)
            {
                case "Radius":
                {
                    var lScale = transform.localScale;
                    lScale.x *=  ps.increase;
                    lScale.y *= ps.increase;
                    transform.localScale += lScale;
                    break;
                }
                case "Damage":
                    var cacheDamage = projectileDamage * ps.increase;
                    projectileDamage += cacheDamage;
                    break;
                case "Speed":
                    var cacheSpeed = projectileSpeed * ps.increase;
                    projectileSpeed += cacheSpeed;
                    break;
            }
        }
    }
}
