using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MosMagnet : Projectile
{
    [SerializeField] private GameObject mosRange;
    private List<EnemyStat> enemyStatList = new();
    
    protected override void OnEnable()
    {
        UpdateProjectileStats();
        if (isEvolved)
        {
            // Set new animation controller
            animator.runtimeAnimatorController = evolvedController;
        }
        animator.SetTrigger("Play");
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        if (enemyStatList.Count > 0)
        {
            foreach (EnemyStat es in enemyStatList)
            {
                es.StopDoT();
            }
            enemyStatList.Clear();
        }
        StopAllCoroutines();
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent(out EnemyStat enemyStat))
        {
            enemyStatList.Add(enemyStat);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out EnemyStat enemyStat))
        {
            //enemyStat.StopDoT();
            if (enemyStatList.Contains(enemyStat))
            {
                enemyStatList.Remove(enemyStat);
            }
        }
    }

    protected override IEnumerator SelfDestructTimer()
    {
        yield return new WaitForSeconds(destructTimer);
        SingletonManager.Get<AudioManager>().StopSFX("Magnet OnLoop");
        SingletonManager.Get<AudioManager>().PlaySFX("Magnet Off");
        OnDeactivate();
    }
    
    private IEnumerator DamageAll()
    {
        while (true)
        {
            for (var index = 0; index < enemyStatList.Count; index++)
            {
                var es = enemyStatList[index];
                if (es.isActiveAndEnabled)
                {
                    // Don't forget to count for their resistances
                    es.TakeDamage(projectileDamage, 0);
                    if (!es.isActiveAndEnabled)
                    {
                        enemyStatList.Remove(es);
                    }
                }
            }
            if (enemyStatList.Count > 0)
            {
                SingletonManager.Get<AudioManager>().PlaySFX("Enemy Damage");
            }
            yield return new WaitForSeconds(damageTimeTick);
        }
    }

    public void StartDamageCo()
    {
        StartCoroutine(DamageAll());
    }

    public void StartDestructCo()
    {
        StartCoroutine(SelfDestructTimer());
    }
    
    private void OnDeactivate()
    {
        animator.SetTrigger("Offline");
        if (GetComponent<CircleCollider2D>())
        {
            GetComponent<CircleCollider2D>().enabled = false;
        }
        mosRange.SetActive(false);
    }
    
    protected override void UpdateProjectileStats()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                isEvolved = ws.isEvolved;
                
                // Scale
                transform.localScale = new Vector3(ws.weaponLevelSO.defaultSize.x, ws.weaponLevelSO.defaultSize.y, 0);
                var lScale = transform.localScale;
                lScale.x *=  ws.sizeIncrease;
                lScale.y *= ws.sizeIncrease;
                transform.localScale += lScale;
                
                // Damage
                projectileDamage = ws.weaponLevelSO.defaultDamage;
                var cacheDamage = projectileDamage * ws.damageIncrease;
                projectileDamage += cacheDamage;
                
                // Damage Time Tick
                damageTimeTick = ws.weaponLevelSO.defaultDamageTick;
                var cacheDamageTick = damageTimeTick * ws.damageTickIncrease;
                damageTimeTick -= cacheDamageTick;
                
                // Destruct Timer
                destructTimer = ws.weaponLevelSO.defaultDestructTimer;
                var cacheDestructTimer = destructTimer * ws.destructTimerIncrease;
                destructTimer += cacheDestructTimer;
            }
        }
        foreach (PowerupStat ps in SingletonManager.Get<PowerupsManager>().powerups)
        {
            switch (ps.powerupName)
            {
                case "Radius":
                    var lScale = transform.localScale;
                    lScale.x *=  ps.increase;
                    lScale.y *= ps.increase;
                    transform.localScale += lScale;
                    break;
                case "Damage":
                    var cacheDamage = projectileDamage * ps.increase;
                    projectileDamage += cacheDamage;
                    break;
                case "Time":
                    var cacheDestructTimer = destructTimer * ps.increase;
                    destructTimer += cacheDestructTimer;
                    break;
            }
        }
    }
}
