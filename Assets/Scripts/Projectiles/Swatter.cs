using UnityEngine;

public class Swatter : Projectile
{

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent(out EnemyStat enemyStat))
        {
            SingletonManager.Get<AudioManager>().PlaySFX("Enemy Electric Damage");
            enemyStat.TakeDamage(projectileDamage, 0);
        }
    }

    protected override void UpdateProjectileStats()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                isEvolved = ws.isEvolved;
                
                // Scale
                transform.localScale = new Vector3(ws.weaponLevelSO.defaultSize.x, ws.weaponLevelSO.defaultSize.y, 0);
                var lScale = transform.localScale;
                lScale.x *=  ws.sizeIncrease;
                lScale.y *= ws.sizeIncrease;
                transform.localScale += lScale;
                
                // Damage
                projectileDamage = ws.weaponLevelSO.defaultDamage;
                var cacheDamage = projectileDamage * ws.damageIncrease;
                projectileDamage += cacheDamage;
                
                // Destruct Timer
                destructTimer = ws.weaponLevelSO.defaultDestructTimer;
                var cacheDestructTimer = destructTimer * ws.destructTimerIncrease;
                destructTimer += cacheDestructTimer;
            }
        }
        foreach (PowerupStat ps in SingletonManager.Get<PowerupsManager>().powerups)
        {
            switch (ps.powerupName)
            {
                case "Radius":
                {
                    var lScale = transform.localScale;
                    lScale.x *=  ps.increase;
                    lScale.y *= ps.increase;
                    transform.localScale += lScale;
                    break;
                }
                case "Damage":
                    var cacheDamage = projectileDamage * ps.increase;
                    projectileDamage += cacheDamage;
                    break;
            }
        }
    }
}
