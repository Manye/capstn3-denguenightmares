using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class FactoidRandomizer : MonoBehaviour
{
    [SerializeField] private List<string> factoids;
    [SerializeField] private TextMeshProUGUI factoidContainer;

    private int counter = 0;
    private void Start()
    {
        SetRandomFactoid();
    }
    public void SetRandomFactoid() 
    {
        //5
        //counter = 4,
        // counter >= 4
        if (counter >= factoids.Count) counter = 0;
        factoidContainer.text = factoids[counter];
        counter++;
    }

}
