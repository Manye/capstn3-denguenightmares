using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [Header(DS_Constants.ASSIGNABLE)]
    [SerializeField] protected EnemyStat enemyStat;
    [SerializeField] protected GameObject enemyParent;

    [Header(DS_Constants.DO_NOT_ASSIGN)] 
    protected PlayerStat pStat;
    [SerializeField] protected float playerDef;
    [SerializeField] protected float currentDamage;
    [SerializeField] protected float currentAtkSpeed;

    private Coroutine damageCo;
    private Coroutine destructionCo;
    
    protected virtual void Start()
    {
        playerDef = 0;
        currentDamage = enemyStat.statSO.damage;
        currentAtkSpeed = enemyStat.statSO.atkSpeed;
    }

    private void OnEnable()
    {
        damageCo = StartCoroutine(DamageCo());
    }

    private void OnDisable()
    {
        if (damageCo != null)
        {
            StopCoroutine(damageCo);
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.TryGetComponent(out PlayerStat playerStat))
        {
            pStat = playerStat;
        }

        if (col.GetComponent<Camera>())
        {
            if (destructionCo != null){ StopCoroutine(destructionCo);}
        }
    }
    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out PlayerStat playerStat))
        {
            pStat = null;   
        }
        
        if (collision.GetComponent<Camera>())
        {
            if (enemyParent)
            {
                if (enemyParent.activeInHierarchy)
                    destructionCo = StartCoroutine(CountdownToDestruction());
            }
        }
    }

    protected virtual IEnumerator DamageCo()
    {
        while (true)
        {
            if (pStat != null)
            {
                pStat.TakeDamage(currentDamage, playerDef);
                SingletonManager.Get<AudioManager>().PlaySFX("Player Damage");
            }
            yield return new WaitForSeconds(currentAtkSpeed);
        }
    }
    
    public void UpdateAttackSpeed(float atkSpeed)
    {
        currentAtkSpeed = atkSpeed;
    }

    private void SetParentActive(bool p_bool)
    {
        if (enemyParent) enemyParent.SetActive(p_bool);
        else enemyParent.SetActive(p_bool);
    }


    private IEnumerator CountdownToDestruction()
    {
        float targetTime = SingletonManager.Get<SM_Enemy>().destroyTime;
        yield return new WaitForSeconds(targetTime);
        SetParentActive(false);
    }
    
}