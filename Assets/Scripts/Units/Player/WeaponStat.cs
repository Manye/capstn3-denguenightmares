using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[System.Serializable]
public class WeaponStat
{
    public string weaponName;
    public SpawnManager weaponSM;
    public PowerupStat PowerupStat;
    public SO_WeaponLevel weaponLevelSO;

    public bool isEvolved;
    public int level;
    [FormerlySerializedAs("speed")] public float speedIncrease;
    [FormerlySerializedAs("size")] public float sizeIncrease;
    [FormerlySerializedAs("damage")] public float damageIncrease;
    [FormerlySerializedAs("damageTick")] public float damageTickIncrease;
    [FormerlySerializedAs("attackRate")] public float attackRateIncrease;
    [FormerlySerializedAs("destructTimer")] public float destructTimerIncrease;

    //Added Parameters
    public int maxLevel = 5; // Level 6
    public SO_Rewards rewardSO;
}
