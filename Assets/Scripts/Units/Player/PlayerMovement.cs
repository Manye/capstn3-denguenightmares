using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header(DS_Constants.ASSIGNABLE)]
    [SerializeField] private Animator animator;
    [Header(DS_Constants.DO_NOT_ASSIGN)]
    [SerializeField] private float currentSpeed;
    [SerializeField] private bool isFacingRight = true;
    [SerializeField] private Vector2 movement;
    [SerializeField] private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        currentSpeed = SingletonManager.Get<GameManager>().player.GetComponent<PlayerStat>().statSO.moveSpeed; // Set Player Movement Speed
    }

    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
    }

    private void FixedUpdate()
    {
        Vector2 movement = new Vector2(this.movement.x, this.movement.y).normalized;
        rb.MovePosition(rb.position + movement * (currentSpeed * Time.deltaTime));

        if (movement.magnitude > 0)
        {
            // Play moving animation
            animator.SetTrigger("isWalk");
        }
        else
        {
            // Play idle animation
            animator.SetTrigger("isIdle");
        }

        if (movement.x > 0 && !isFacingRight) Flip(); //Face Right if Movement X is Greater than 0
        if (movement.x < 0 && isFacingRight) Flip(); //Face Right if Movement X is Less than 0
    }
    
    #region Functions
    private void Flip()
    {
        Vector3 currentScale = gameObject.transform.localScale;
        currentScale.x *= -1;
        gameObject.transform.localScale = currentScale;
        isFacingRight = !isFacingRight;
    }
    
    public void UpdateMovementSpeed(float speed) => currentSpeed = speed; // Updates speed when Player hits an upgrade
    #endregion
}
