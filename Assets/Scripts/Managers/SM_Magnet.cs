using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SM_Magnet : SpawnManager
{
    private int transformCounter = 0;

    protected override void Spawn()
    {
        SingletonManager.Get<AudioManager>().PlaySFX("Magnet On");
        SingletonManager.Get<AudioManager>().PlaySFX("Magnet OnLoop");
        for (int i = 0; i < spawnDuplicator; i++)
        {
            Transform t = spawnTransforms[transformCounter];
            transformCounter++;
            GameObject go = objectPooler.SpawnFromPool(poolSO.pool.tag, new Vector3(t.position.x, t.position.y),
                Quaternion.identity);
            if (transformCounter >= spawnTransforms.Length)
            {
                transformCounter = 0;
            }
        }
    }
    
    protected override void UpdateWeapon()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                // Spawn Rate
                level = ws.level;
                spawnRate = ws.weaponLevelSO.defaultAttackRate;
                var cacheSpawnRate = spawnRate * ws.attackRateIncrease;
                spawnRate -= cacheSpawnRate;
                
                if (ws.level > 5)
                {
                    Evolve();
                }
                break;
            }
        }
        foreach (PowerupStat ps in SingletonManager.Get<PowerupsManager>().powerups)
        {
            switch (ps.powerupName)
            {
                case "AtkRate":
                    var cacheSpawnRate = spawnRate * ps.increase;
                    spawnRate -= cacheSpawnRate;
                    break;
                case "Duplicator":
                    spawnDuplicator = 1; // by default it should spawn one (1).
                    if (ps.level >= 2)
                    {
                        spawnDuplicator += (int)ps.increase;
                    }
                    break;
            }
        }
    }
}
