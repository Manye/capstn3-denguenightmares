using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SM_Katol : SpawnManager
{
    protected override void Start()
    {
        SingletonManager.Get<GameManager>().onUpdateWeaponsEvent.AddListener(UpdateWeapon);
    }
    
    protected override void UpdateWeapon()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                if (ws.level > 5)
                {
                    Evolve();
                }
                break;
            }
        }
    }
}
