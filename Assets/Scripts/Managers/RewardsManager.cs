using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class RewardsManager : MonoBehaviour
{
    [Header(DS_Constants.DO_NOT_ASSIGN)] 
    private Dictionary<WeaponStat, PowerupStat> wppuDictionary = new();
    [SerializeField] private List<SO_Rewards> rewardsCache;

    [Header(DS_Constants.ASSIGNABLE)]
    [SerializeField] private List<GameObject> buttonChoices;

    [SerializeField] private SO_Rewards pointsReward;
    [SerializeField] private List<SO_Rewards> staticRewardsList;

    [SerializeField] private LevelUpPanelScript levelUpPanel;
    [SerializeField] private float levelForPoints;
    private PlayerStat playerStat;
    private void Start()
    {
        SingletonManager.Get<GameManager>().onLevelUpEvent.AddListener(ScanItems);
       
        var wses = SingletonManager.Get<WeaponsManager>().weapons;
        var pses = SingletonManager.Get<PowerupsManager>().powerups;
        for (var i = 0; i < 7; i++)
        {
            wppuDictionary.Add(wses[i], pses[i]);
        }
    }
    
    private void OnDisable()
    {
        SingletonManager.Get<GameManager>().onLevelUpEvent.RemoveListener(ScanItems);
    }

    private void ScanItems(bool p_bool)
    {
        if (!playerStat) playerStat = FindObjectOfType<PlayerStat>();

        if (playerStat.level >= levelForPoints) 
        {
            rewardsCache.Clear();
            OfferPointsUI();
            return;
        }

        // Initialize and transfer all staticRewardsList to rewardsCache.
        foreach(SO_Rewards rw in staticRewardsList) rewardsCache.Add(rw);
        
        int activeWeaponCounter = 0;
        int maxLvlWeaponCounter = 0;
        int activePowerUpCounter = 0;
        int maxLevelPowerUpCounter = 0;

        //Get active weapon count
        foreach (var (ws, ps) in wppuDictionary) 
        {
            if (ws.weaponSM.gameObject.activeInHierarchy) // Checks if the Weapon is Active
                activeWeaponCounter++;
            if (ps.level > 0)
                activePowerUpCounter++;
        }

        foreach (var (ws, ps) in wppuDictionary)
        {
            if (ws.level > 5) maxLvlWeaponCounter++;
            if (ps.level >= 3) maxLevelPowerUpCounter++;

            // Get active weapon count
            // Weapons Checker
            if (ws.weaponSM.gameObject.activeInHierarchy) // Checks if the Weapon is Active
            {
                if (activeWeaponCounter >= 5)  // Checks if Player has Max Weapon Count (x5) 
                {
                    Debug.Log(activeWeaponCounter + " active weapons");
                    if (ws.isEvolved) // Check if the Weapon is Evolved or Level 5
                        rewardsCache.Remove(GetStaticReward(ws.weaponLevelSO, null)); // Remove Weapon if Evolved or Level 5 and above
                }
                else 
                {
                    if (ws.isEvolved) // Check if the Weapon is Evolved or Level 5
                        rewardsCache.Remove(GetStaticReward(ws.weaponLevelSO, null)); // Remove Weapon if Evolved or Level 5 and above
                }

            }
            else // Checks if the Weapon is Inactive
            {
                if(activeWeaponCounter >= 5) // Checks if the Player has maximized his Weapon Inventory
                    rewardsCache.Remove(GetStaticReward(ws.weaponLevelSO, null)); // Remove Weapon if Evolved or Level 5 and above

            }

            // Powerups Checker
            if (ps.level >= 1)
            {
                if (ps.level >= 3)
                {
                    Debug.Log("Removed " + ps.powerupName);
                    rewardsCache.Remove(GetStaticReward(null, ps.powerupLevelSO));
                }
                else
                {
                    Debug.Log("Retained " + ps.powerupName + " Level : " + ps.level);
                }
            }
            else 
            {
                if(SingletonManager.Get<InventoryController>().powerUpFull()) 
                    rewardsCache.Remove(GetStaticReward(null, ps.powerupLevelSO));
            }
        }
        
        // If all weapons and powerups are maxed out; offer points instead.
        if (maxLvlWeaponCounter >= 5 && maxLevelPowerUpCounter >= 5)
        {
            rewardsCache.Clear();
            OfferPointsUI();
        }
        else OfferRewardsUI();
    }

    // Pairing and  Check for Instant Leveling
    private WeaponStat getWeaponWithInstantLevelUp(WeaponStat weaponStat)
    {
        PowerupStat powerUpStat = weaponStat.PowerupStat;

        if (powerUpStat.powerupName.Equals("")) return null; // Returns if there is not PowerUp Stat
        if (weaponStat.isEvolved) return null; // Returns if the weapon is already Evolved

         /*
         * Maximum Level is 6 which is evolved. Subtract 4 from max level to get a difference of 2, which equates
         * to Level 3
         */

        if (weaponStat.level >= weaponStat.maxLevel - 4 &&
            powerUpStat.level >= powerUpStat.maxLevel - 1)
        {
            return weaponStat;
        }
        else return null;
    }

    /// <summary>
    /// Gets the PowerUp Stat in order for us to get the PowerUpStat and add it to the onClick Listener upon
    /// Player selecting an upgrade (PowerUp)
    /// </summary>
    /// <param name="powerUpLevels"></param>
    /// <returns></returns>
    private PowerupStat getPowerUpStat(SO_PowerupLevels powerUpLevels) 
    {
        foreach (var (ws, ps) in wppuDictionary)
        {
            if (powerUpLevels.Equals(ps.powerupLevelSO))
            {
                return ps;
            }
        }

        return null;
    }

    private WeaponStat getWeaponStat(SO_WeaponLevel weaponLevel)
    {
        foreach (var (ws, ps) in wppuDictionary)
        {
            if (weaponLevel.Equals(ws.weaponLevelSO))
            {
                return ws;
            }
        }

        return null;
    }
    private void OfferRewardsUI()
    {
        if (!levelUpPanel.gameObject.activeSelf) levelUpPanel.gameObject.SetActive(true);

        levelUpPanel.TruncateChoices();

        int choicesToSet = 3 - levelUpPanel.getSpawnChoicesCount(); // Gets the number of choices to be set

        for (int i = 0; i < choicesToSet; i++)
        {
            int randomize = Random.Range(0, rewardsCache.Count); // Gets a randomized integer based on the Static Rewards List

            var randomizedReward = rewardsCache[randomize];

            if (randomizedReward.powerupLevelSO) // Check if the Randomized Reward is a Power Up
            {
                PowerupStat powerUpStat = getPowerUpStat(randomizedReward.powerupLevelSO);

                if (levelUpPanel.CheckDuplicates(null, powerUpStat)) i--; // Restart the current index of the loop if there's a duplicate
                else
                {
                    levelUpPanel.SpawnPowerUpChoice(powerUpStat);
                    SetOnPowerUpSelectListeners(levelUpPanel.getCurrentButton(), powerUpStat);
                }
            }
            else // Check if the Randomized Reward is a Weapon
            {
                WeaponStat weaponStat = getWeaponStat(randomizedReward.weaponLevelSO);

                if (levelUpPanel.CheckDuplicates(weaponStat, null)) i--; // Restart the current index of the loop if there's a duplicate
                else
                {
                    levelUpPanel.SpawnWeaponChoice(weaponStat);
                    SetOnWeaponOnSelectListeners(levelUpPanel.getCurrentButton(), weaponStat);
                }
            }
        }
    }

    private void SetOnWeaponOnSelectListeners(Button button,WeaponStat weaponStat)
    {
        int j = GetStaticRewardIndex(weaponStat.rewardSO);
        
        button.onClick.AddListener(() => SingletonManager.Get<WeaponsManager>().LevelUp(staticRewardsList[j].weaponLevelSO));
        button.onClick.AddListener(() => SingletonManager.Get<GameManager>().UpdateUpgrades());
        button.onClick.AddListener(() => SingletonManager.Get<InventoryController>().OnAddWeapon(weaponStat));
        button.onClick.AddListener(() => rewardsCache.Clear());
    }

    private void SetOnPowerUpSelectListeners(Button button, PowerupStat powerUpStat)
    {
        int j = GetStaticRewardIndex(powerUpStat.rewardSO);

        button.onClick.AddListener(() => OnSelectPowerUpStat(getPowerUpStat(powerUpStat.rewardSO.powerupLevelSO)));
        button.onClick.AddListener(() => SingletonManager.Get<PowerupsManager>().LevelUp(staticRewardsList[j].powerupLevelSO));
        button.onClick.AddListener(() => SingletonManager.Get<GameManager>().UpdateUpgrades()); 
        button.onClick.AddListener(() => SingletonManager.Get<InventoryController>().OnAddPowerUp(powerUpStat));

        button.onClick.AddListener(() => rewardsCache.Clear());
    }

    private void SetOnPointSelectListeners(Button button) 
    {
        button.onClick.AddListener(() => SingletonManager.Get<GameManager>().IncrementPoints(100));
    }
    private void OnSelectPowerUpStat(PowerupStat powerupStat)
    {
        foreach (WeaponStat weaponStat in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (powerupStat.targetWeapon == weaponStat) 
            {
                if (weaponStat.PowerupStat.powerupName.Equals(""))
                {
                    weaponStat.PowerupStat = powerupStat;
                    break;
                }  
            }
        }
    }

    private void OfferPointsUI()
    {
        levelUpPanel.TruncateChoices();

        for (int i = 0; i < 3 ; i++)
        {
            levelUpPanel.SpawnPointsChoice();
            SetOnPointSelectListeners(levelUpPanel.getCurrentButton());
            
        }
    }
    
    private SO_Rewards GetStaticReward(SO_WeaponLevel weaponLevelSO, SO_PowerupLevels powerupLevelSO)
    {
        if (weaponLevelSO)
        {
            foreach (var rw in staticRewardsList)
            {
                if (weaponLevelSO.Equals(rw.weaponLevelSO))
                {
                    return rw;
                }
            }
        }
        else if (powerupLevelSO)
        {
            foreach (var rw in staticRewardsList)
            {
                if (powerupLevelSO.Equals(rw.powerupLevelSO))
                {
                    return rw;
                }
            }
        }
        return null;
    }

    private int GetStaticRewardIndex(SO_Rewards rewardReference)
    {
        for (int i = 0; i < staticRewardsList.Count; i++)
        {
            if (rewardReference.Equals(staticRewardsList[i]))
            {
                return i;
            }
        }
        return -1;
    }
}