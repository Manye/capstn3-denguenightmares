using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header(DS_Constants.DO_NOT_ASSIGN)] 
    public bool isMenuOpen;
    
    [Header(DS_Constants.ASSIGNABLE)]
    public GameManager gameManager;
    [SerializeField] private PlayerStat playerStat;
    private HealthBar healthBar;
    public Slider healthSlider;
    public TextMeshProUGUI healthText;

    public GameObject characterSelectPanel;
    public GameObject pausePanel;
    public GameObject levelUpPanel;
    public GameObject gameWinPanel;
    public GameObject gameOverPanel;

    public TextMeshProUGUI timerText;
    private XPBar xpBar; 
    public Slider xpSlider;
    public TextMeshProUGUI LevelText;
    public TextMeshProUGUI objectiveCounterText;
    public TextMeshProUGUI enemyCounterText;
    public TextMeshProUGUI enemyKillCountText;
    public TextMeshProUGUI playerPointsCountText;
    
    public Material whiteFlashMat;
    public float flashDuration;

    
    public Color colorNew;
    public Color colorEvolved;
    public Color colorLevelUp;

    private int tutorSpriteIterator;
    public GameObject tutorPanel;
    public GameObject tutorNextButton;
    public Image tutorRef;
    public List<Sprite> tutorialImages;
    
    public OnUpdateUIXP onUpdateUIXP = new();
    
    private void Awake()
    {
        SingletonManager.Register(this);
    }
    
    private void OnEnable()
    {
        healthBar = healthSlider.GetComponent<HealthBar>();
        healthBar.slider = healthSlider;
        gameManager.onUpdateUpgradesEvent.AddListener(() => UpdateHPUI(gameManager.player.GetComponent<PlayerStat>().unitHealth.health));
        playerStat.unitHealth.onDamageEvent.AddListener(UpdateHPUI);
        
        xpBar = xpSlider.GetComponent<XPBar>();
        xpBar.slider = xpSlider;
        onUpdateUIXP.AddListener(UpdateXPUI);
        gameManager.onGamePauseEvent.AddListener(PauseUI);
        gameManager.onLevelUpEvent.AddListener(OpenLevelUpUI);
        gameManager.onPlayerWinEvent.AddListener(PlayerWinUI);
        playerStat.unitHealth.onDeathEvent.AddListener(PlayerLoseUI);
    }

    private void Start()
    {
        UpdateEnemyCountUI(gameManager.enemyCounter);
        UpdateObjectivesUI();
        ReferenceObjectivesUI();
    }

    private void OnDisable()
    {
        gameManager.onUpdateUpgradesEvent.RemoveListener(() => UpdateHPUI(gameManager.player.GetComponent<PlayerStat>().unitHealth.health));
        playerStat.unitHealth.onDamageEvent.RemoveListener(UpdateHPUI);
        onUpdateUIXP.RemoveListener(UpdateXPUI);
        gameManager.onGamePauseEvent.RemoveListener(PauseUI);
        gameManager.onLevelUpEvent.RemoveListener(OpenLevelUpUI);
        gameManager.onPlayerWinEvent.RemoveListener(PlayerWinUI);
    }

    #region Tutorial

    public void TutorialNextImage()
    {
        if (tutorSpriteIterator < tutorialImages.Count - 1)
        {
            tutorSpriteIterator++;
            tutorRef.sprite = tutorialImages[tutorSpriteIterator];
        }
        else
        {
            SingletonManager.Get<GameManager>().PauseGameTime(false);
            tutorPanel.SetActive(false);
            SingletonManager.Get<AudioManager>().PlaySFX("Button Click Back");
            tutorSpriteIterator = 0;
        }
    }
    public void ShowControls()
    {
        tutorPanel.SetActive(true);
        tutorNextButton.SetActive(false);
        tutorRef.sprite = tutorialImages[1];
    }

    #endregion

    private void PauseUI(bool p_bool)
    {
        pausePanel.SetActive(p_bool);
    }
    
    private void PlayerWinUI()
    {
        gameWinPanel.SetActive(true);
        isMenuOpen = true;
    }
    
    private void PlayerLoseUI()
    {
        gameOverPanel.SetActive(true);
        isMenuOpen = true;
    }

    public void OnCharacterSelected()
    {
        isMenuOpen = false;
        healthBar.SetMaxHealth(playerStat.unitHealth.maxHealth);
        UpdateHPUI(playerStat.unitHealth.health);
        UpdateXPUI(playerStat.level, playerStat.currentXP, playerStat.requiredXP);
    }

    private void ReferenceObjectivesUI()
    {
        for (int i = 0; i < gameManager.objectiveParent.transform.childCount; i++)
        {
            var child = gameManager.objectiveParent.transform.GetChild(i);
            for (int j = 0; j < child.transform.childCount; j++)
            {
                var grandChild = child.transform.GetChild(j);

                if(grandChild.transform.GetComponent<IO_Water>())
                    grandChild.transform.GetComponent<IO_Water>().onInteractEvent.AddListener(UpdateObjectivesUI);
            }
        }
    }
    private void UpdateObjectivesUI()
    {
        objectiveCounterText.text = $"Stagnant Water Left: {gameManager.objectiveCounter}";
    }

    public void UpdateHPUI(float currentHealth)
    {
        healthBar.SetHealth(currentHealth);
        healthText.text = "HP: " + Mathf.RoundToInt(currentHealth) + " / " + playerStat.statSO.maxHealth;
    }
    
    private void UpdateXPUI(int level, float currentXP, float requiredXP)
    {
        xpBar.SetMaxXP(currentXP, requiredXP);
        LevelText.text = "LVL " + level;
    }

    public void UpdateEnemyCountUI(int amount)
    {
        enemyCounterText.text = amount.ToString();
    }

    public void UpdateEnemyKillCountUI(int amount)
    {
        enemyKillCountText.text = amount.ToString();
    }

    public void UpdatePointsUI(int points)
    {
        playerPointsCountText.text = points.ToString();
    }

    public void OpenLevelUpUI(bool p_bool)
    {
        levelUpPanel.SetActive(p_bool);
        isMenuOpen = p_bool;
    }
}
