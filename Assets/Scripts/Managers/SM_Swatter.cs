using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.Serialization;

public class SM_Swatter : SpawnManager
{
    protected override void Spawn()
    {
        StartCoroutine(SpawnOnLevelCoroutine(level));
    }
    protected override void UpdateWeapon()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                level = ws.level;
                // Spawn Rate
                spawnRate = ws.weaponLevelSO.defaultAttackRate;
                var cacheSpawnRate = spawnRate * ws.attackRateIncrease;
                spawnRate -= cacheSpawnRate;
                if (ws.level > 5)
                {
                    Evolve();
                }
                break;
            }
        }
        foreach (PowerupStat ps in SingletonManager.Get<PowerupsManager>().powerups)
        {
            switch (ps.powerupName)
            {
                case "AtkRate":
                    var cacheSpawnRate = spawnRate * ps.increase;
                    spawnRate -= cacheSpawnRate;
                    break;
            }
        }
    }

    private IEnumerator SpawnOnLevelCoroutine(int level)
    {
        switch (level)
        {
            case 1:
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[0].position, Quaternion.identity);
                break;
            case 2:
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[0].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[1].position, Quaternion.identity);
                break;
            case 3:
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[0].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[1].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[3].position, Quaternion.identity);
                break;
            case 4:
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[0].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[1].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[2].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[3].position, Quaternion.identity);
                break;
            case 5:
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[0].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[1].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[2].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[3].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[4].position, Quaternion.identity);
                break;
            case 6:
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[0].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[1].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[2].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[3].position, Quaternion.identity);
                yield return new WaitForSeconds(0.5f);
                SingletonManager.Get<AudioManager>().PlaySFX("Swatter OnFire");
                objectPooler.SpawnFromPool(poolSO.pool.tag, spawnTransforms[4].position, Quaternion.identity);
                break;
        }
    }
}
