using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private bool isInMainGameScene;
    
    [Header("Audio Components")]
    public Sound[] bgmSounds; 
    public Sound[] sfxSounds;

    [Header("Audio Sources")]
    public AudioSource musicSource;
    public AudioSource sfxSource;

    [Header("Audio Mixers")]
    [SerializeField] AudioMixer mainMixer;
    public AudioMixerGroup bgmMixer;
    public AudioMixerGroup sfxMixer;

    public string BGM_KEY = "BGMVolume";
    public string SFX_KEY = "SFXVolume";


    void Awake()
    {
        SingletonManager.Register(this);

        foreach (Sound s in bgmSounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.loop = s.isLooping;
            s.source.outputAudioMixerGroup = bgmMixer;
        }
        foreach (Sound s in sfxSounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.loop = s.isLooping;
            s.source.outputAudioMixerGroup = sfxMixer;
        }


        if (!isInMainGameScene)
        {
            LoadVolume();
        }
    }

    // play for the bgm music
    public void PlayBGM(string bgmName)
    {
        Sound s = Array.Find(bgmSounds, bgm => bgm.audioName == bgmName);

        if (s == null)
        {
            Debug.Log("Sound Not Found!");
            return;
        }
        else
        {
            s.source.PlayOneShot(s.clip);
        }

    }

    public void PlayRandomBGM()
    {
        int randNum = Random.value > 0.5f ? 0 : 1;
        Sound s = bgmSounds[randNum];
        if (s == null)
        {
            Debug.Log("Sound Not Found!");
        }
        else s.source.Play();
    }

    public void StopTwoBGM()
    {
        for (int i = 0; i < 2; i++)
        {
            Sound s = bgmSounds[i];
            if (s.source.isPlaying)
            {
                s.source.Stop();
            }
        }
    }

    public void StopBGM(string bgmName)
    {
        Sound s = Array.Find(bgmSounds, bgm => bgm.audioName == bgmName);

        if (s == null)
        {
            Debug.Log("Sound Not Found!");
            return;
        }

        else
        {
            s.source.Stop();
        }

    }

    public void PauseBGM(string bgmName)
    {
        Sound s = Array.Find(bgmSounds, bgm => bgm.audioName == bgmName);

        if (s == null)
        {
            Debug.Log("Sound Not Found!");
            return;
        }

        else
        {
            s.source.Pause();
        }
    }

    public void UnPauseBGM(string bgmName)
    {
        Sound s = Array.Find(bgmSounds, bgm => bgm.audioName == bgmName);

        if (s == null)
        {
            Debug.Log("Sound Not Found!");
            return;
        }

        else
        {
            s.source.UnPause();
        }
    }


    public void PlaySFX(string sfxName)
    {
        Sound s = Array.Find(sfxSounds, sfx => sfx.audioName == sfxName);

        if (s == null)
        {
            Debug.Log("Sound Not Found!");
            return;
        }

        else
        {
            s.source.PlayOneShot(s.clip);
        }

    }

    public void StopSFX(string sfxName)
    {
        Sound s = Array.Find(sfxSounds, sfx => sfx.audioName == sfxName);

        if (s == null)
        {
            Debug.Log("Sound Not Found!");
            return;
        }

        else
        {
            s.source.Stop();
        }
    }

    public void ToggleMusic()
    {
        musicSource.mute = !musicSource.mute;
    }

    public void ToggleSFX()
    {
        sfxSource.mute = !sfxSource.mute;
    }

    public void AdjustMusicVolume(float volume)
    {
        musicSource.volume = volume;
    }

    public void AdjustSFXVolume(float volume)
    {
        sfxSource.volume = volume;
    }

    public float GetAudioLength(string sfxName)
    {
        Sound s = Array.Find(sfxSounds, sfx => sfx.audioName == sfxName);
        return s.clip.length;
    }

    void LoadVolume() // Volume saved in VolumeSettings.cs
    {
        float bgmVolume = PlayerPrefs.GetFloat(BGM_KEY, 1f);
        float sfxVolume = PlayerPrefs.GetFloat(SFX_KEY, 1f);

        mainMixer.SetFloat(VolumeSettings.MIXER_BGM, MathF.Log10(bgmVolume) * 20);
        mainMixer.SetFloat(VolumeSettings.MIXER_SFX, Mathf.Log10(sfxVolume) * 20);
    }

}
