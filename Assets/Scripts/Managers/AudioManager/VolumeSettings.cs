using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.Serialization;

public class VolumeSettings : MonoBehaviour
{
    [SerializeField] private bool isInMainGameScene;
    
    [SerializeField] AudioMixer mixer;
    [SerializeField] Slider bgmSlider;
    [SerializeField] Slider sfxSlider;

    public const string MIXER_BGM = "BGMVolume";
    public const string MIXER_SFX = "SFXVolume";

    void Awake()
    {
        if (!isInMainGameScene)
        {
            bgmSlider.onValueChanged.AddListener(SetBGMVolume);
            sfxSlider.onValueChanged.AddListener(SetSFXVolume);
        }
    }

    void Start()
    {
        if (!isInMainGameScene)
        {
            bgmSlider.value = PlayerPrefs.GetFloat(SingletonManager.Get<AudioManager>().BGM_KEY, 1f);
            sfxSlider.value = PlayerPrefs.GetFloat(SingletonManager.Get<AudioManager>().SFX_KEY, 1f);
        }
    }

    void OnDisable()
    {
        if (!isInMainGameScene)
        {
            PlayerPrefs.SetFloat(SingletonManager.Get<AudioManager>().BGM_KEY, bgmSlider.value);
            PlayerPrefs.SetFloat(SingletonManager.Get<AudioManager>().SFX_KEY, sfxSlider.value);
        }
    }

    void SetBGMVolume(float value)
    {
        mixer.SetFloat(MIXER_BGM, Mathf.Log10(value) * 20);
    }

    void SetSFXVolume(float value)
    {
        mixer.SetFloat(MIXER_SFX, Mathf.Log10(value) * 20);
    }

}
