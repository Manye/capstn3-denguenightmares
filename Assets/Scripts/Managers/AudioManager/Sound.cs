using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound
{
    public string audioName;
    public AudioClip clip;

    public bool isLooping;
    [Range(0f, 1f)]
    public float volume;

    [HideInInspector] public AudioSource source;
    [HideInInspector] public AudioMixerGroup mixerGroup;
}
