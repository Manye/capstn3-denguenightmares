using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class SpawnManager : MonoBehaviour
{
    [Header(DS_Constants.DO_NOT_ASSIGN)] 
    public bool isEvolved = false;
    protected int level = 0;
    [SerializeField] protected ObjectPooler objectPooler;
    [SerializeField] protected float spawnRate;
    protected int spawnDuplicator = 1;
    protected Transform spawnT;

    [Header(DS_Constants.ASSIGNABLE)]
    [SerializeField] protected SO_Pool poolSO;
    [SerializeField] protected Transform[] spawnTransforms;

    protected virtual void Start()
    {
        SingletonManager.Get<GameManager>().onUpdateWeaponsEvent.AddListener(UpdateWeapon);
        SingletonManager.Get<GameManager>().UpdateUpgrades();
        objectPooler = SingletonManager.Get<ObjectPooler>();
        objectPooler.CreatePool(poolSO);
        StartCoroutine(SpawnCoroutine());
    }

    protected virtual IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnRate);
            Spawn();
        }
    }
    
    protected virtual void Spawn()
    {
        Transform randT = spawnTransforms[Random.Range(0, spawnTransforms.Length)];
        GameObject go = objectPooler.SpawnFromPool(poolSO.pool.tag,
            new Vector3(randT.position.x, randT.position.y), Quaternion.identity);
    }

    protected virtual void UpdateWeapon()
    {
        foreach (WeaponStat ws in SingletonManager.Get<WeaponsManager>().weapons)
        {
            if (ws.weaponLevelSO.name.Equals(poolSO.name))
            {
                spawnRate = ws.attackRateIncrease;
                if (ws.level > 5)
                {
                    Evolve();
                }
                break;
            }
        }
        foreach (PowerupStat ps in SingletonManager.Get<PowerupsManager>().powerups)
        {
            switch (ps.powerupName)
            {
                case "AtkRate":
                    spawnRate -= ps.increase;
                    break;
                case "Duplicator":
                    spawnDuplicator += (int)ps.increase;
                    break;
            }
        }
    }

    /*
     * Player selects Swatter
     * Player selects Power Up
     * After up selecting
     * if power up already exists, add level then add
     * Power up checks if the inventory has the target weapon (Swatter)
     * If true , AddStats to the Weapon
     * May it be range,speed,damage
     * 
    */
    protected virtual void Evolve()
    {
        isEvolved = true;
    }
}