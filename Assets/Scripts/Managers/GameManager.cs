using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class GameManager : MonoBehaviour
{
    [Header(DS_Constants.DO_NOT_ASSIGN)] 
    private PlayersScoreBoard scoreBoard;
    [SerializeField] private bool isGamePaused;
    private float gameTime;
    private float cacheStartTime;
    private bool timerActive = false;
    
    //public int xpMultiplier = 0;
    public int playerPoints;
    public int enemyCounter;
    public int enemyKillCounter;
    public int objectiveCounter;
    public GameObject objectiveParent;

    [Header(DS_Constants.ASSIGNABLE)] 
    public UIManager uiManager;
    public GameObject player;
    public List<WeaponStat> startingWeapons;
    public int enemyKillPoints;
    [SerializeField] private GameObject waterClusterPrefab;
    public SpawnManager enemySM;
    [SerializeField] private Animator animator;
    [SerializeField] private List<RuntimeAnimatorController> animationControllers = new();

    public OnTimeCheckEvent OnTimeCheckEvent = new();
    public OnGamePauseEvent onGamePauseEvent = new();
    public OnPlayerWinEvent onPlayerWinEvent = new();
    public OnLevelUpEvent onLevelUpEvent = new();
    public OnUpdateUpgradesEvent onUpdateUpgradesEvent = new();
    public OnUpdateWeaponsEvent onUpdateWeaponsEvent = new();
    public OnEnemySpawnEvent onEnemySpawnEvent = new();
    public OnChangeTargetEvent onChangeTargetEvent = new();
    public OnDeathEvent onEnemyKill = new();

    [Header("Score UI")]
    public TextMeshProUGUI scoreLabel;

    private void Awake()
    {
        SingletonManager.Register(this);
        
        // Reference Objectives when setup.
        ReferenceObjectives();
        StartCoroutine(CheckTime());

    }

    private void OnEnable()
    {
        onLevelUpEvent.AddListener(PlayerLevelUp);
        player.GetComponent<PlayerStat>().unitHealth.onDeathEvent.AddListener(PlayerLose);
        onEnemySpawnEvent.AddListener(EnemySpawnIncrement);
        onEnemyKill.AddListener(DecrementOnEnemyKill);
    }

    private void Start()
    {
        PauseGameTime(true);
        cacheStartTime = Time.time;
        objectiveCounter = DS_Constants.OBJECTIVECOUNT;
        scoreBoard = FindObjectOfType<PlayersScoreBoard>();
        SingletonManager.Get<AudioManager>().StopTwoBGM();
    }

    private void OnDisable()
    {
        onLevelUpEvent.RemoveListener(PlayerLevelUp);
        //player.GetComponent<PlayerStat>().unitHealth.onDeathEvent.RemoveListener(OnPlayerLose);
        onEnemySpawnEvent.RemoveListener(EnemySpawnIncrement);
        onEnemyKill.AddListener(DecrementOnEnemyKill);
    }

    private void Update()
    {
        if (!uiManager.isMenuOpen)
        {
            if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
            {
                SingletonManager.Get<AudioManager>().PlaySFX("Pause");
                PauseGameTime(!isGamePaused);
                onGamePauseEvent.Invoke(isGamePaused);
            }
        }
        
        gameTime = Time.time - cacheStartTime;
        TimeSpan timeSpan = TimeSpan.FromSeconds(gameTime);
        string timerText = string.Format("{0:00}:{1:00}", timeSpan.Minutes, timeSpan.Seconds);
        if (uiManager) uiManager.timerText.text = timerText;
    }

    #region Character Select
    public void OnCharacterSelected(int charNum)
    {
        animator.runtimeAnimatorController = animationControllers[charNum];
        SingletonManager.Get<InventoryController>().OnAddWeapon(startingWeapons[charNum]);
        UpdateUpgrades();
    }
    #endregion
    #region Time
    private IEnumerator CheckTime()
    {
        while (true)
        {
            OnTimeCheckEvent.Invoke((int)gameTime);
            yield return new WaitForSeconds(1f);
        }
    }
    
    public void PauseGameTime(bool p_bool)
    {
        Time.timeScale = p_bool ? 0 : 1;
        isGamePaused = p_bool;
        // Time.timeScale = Time.timeScale >= 1 ? 0 : 1;
    }
    #endregion
    #region LevelUp
    private void PlayerLevelUp(bool p_bool)
    {
        SingletonManager.Get<AudioManager>().PlaySFX("Level Up Appear");
        //PauseGameTime(p_bool);
    }
    public void UpdateUpgrades()
    {
        onUpdateUpgradesEvent.Invoke();
        onUpdateWeaponsEvent.Invoke();
    }
    #endregion
    private void ReferenceObjectives()
    {
        for (int i = 0; i < objectiveParent.transform.childCount; i++)
        {
            var child = objectiveParent.transform.GetChild(i);
            for (int j = 0; j < child.transform.childCount; j++)
            {
                var grandChild = child.transform.GetChild(j);
                if(grandChild.GetComponent<IO_Water>())
                    grandChild.GetComponent<IO_Water>().onInteractEvent.AddListener(PlayerWin); // Add more variable
            }
        }
    }
    #region Win/Lose Conditions
    private void PlayerWin()
    {
        objectiveCounter--;
        // Game Finish!
        if (objectiveCounter <= 0)
        {
            //Debug.Log("Player wins!");
            scoreBoard.CheckForHighScore();
            onPlayerWinEvent.Invoke();
            PauseGameTime(true);
            SingletonManager.Get<AudioManager>().StopTwoBGM();
            SingletonManager.Get<AudioManager>().PlaySFX("Game Win");
            scoreLabel.text = playerPoints.ToString();
        }
    }

    public void WinCheat()
    {
        onPlayerWinEvent.Invoke();
        SingletonManager.Get<AudioManager>().StopTwoBGM();
        SingletonManager.Get<AudioManager>().PlaySFX("Game Win");
        scoreBoard.CheckForHighScore();
        PauseGameTime(true);
        scoreLabel.text = playerPoints.ToString();
    }
    private void PlayerLose()
    {
        SingletonManager.Get<AudioManager>().StopTwoBGM();
        SingletonManager.Get<AudioManager>().PlaySFX("Game Lose");
        PauseGameTime(true);
    }
    public void LoseCheat()
    {
        player.GetComponent<PlayerStat>().unitHealth.onDeathEvent.Invoke();
        PauseGameTime(true);
    }
    #endregion
    #region Point System
    public void IncrementPoints(int amount)
    {
        playerPoints += amount;
    }
    #endregion
    #region Enemy Events
    private void EnemySpawnIncrement()
    {
        enemyCounter++;
        uiManager.UpdateEnemyCountUI(enemyCounter);
    }

    private void DecrementOnEnemyKill()
    {
        enemyCounter--;
        enemyKillCounter++;
        IncrementPoints(enemyKillPoints);
        // Update Enemy Count UI
        uiManager.UpdateEnemyCountUI(enemyCounter);
        // Update Enemy Kill Counter UI
        uiManager.UpdateEnemyKillCountUI(enemyKillCounter);
        // Update Player points UI
        uiManager.UpdatePointsUI(playerPoints);
    }
    #endregion
    #region Reset() and Quit()
    public void GameReset()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GameMainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    public void ApplicationQuit()
    {
        Application.Quit();
    }
    #endregion
}
