using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Interacter : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.TryGetComponent(out IO_Water iObjPool))
        {
            SingletonManager.Get<AudioManager>().PlaySFX("Gain XP");
            GetComponentInParent<PlayerStat>().GainExperienceFlatRate(iObjPool.xpAmount);
            iObjPool.onInteractEvent.Invoke();
        }
    }

}
