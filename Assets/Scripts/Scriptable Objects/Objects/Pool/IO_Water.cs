using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IO_Water : InteractableObject
{
    public float xpAmount;
    
    protected override void OnInteract()
    {
        WaterCluster waterCluster;
        Transform parent = this.transform.parent;

        SingletonManager.Get<AudioManager>().PlaySFX("Clearning Water");
        
        if (parent != null) 
        {
            Debug.Log("Parent " + parent.name);
            if (parent.TryGetComponent(out waterCluster)) waterCluster.CheckRemainingWater();
        }
        Destroy(gameObject);
    }
}
