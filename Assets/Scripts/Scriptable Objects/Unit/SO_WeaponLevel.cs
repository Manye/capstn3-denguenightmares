using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "New Weapon Level Scriptable Object", menuName = "Scriptable Objects/Weapon Level")]
public class SO_WeaponLevel : ScriptableObject
{
    public List<float> projectileSpeedLevels;
    public List<float> projectileSizeLevels;
    public List<float> damageLevels;
    public List<float> damageTickLevels;
    public List<float> spawnRateLevels;
    public List<float> destructTimerLevels;

    public float defaultSpeed;
    public Vector3 defaultSize;
    public float defaultDamage;
    public float defaultDamageTick;
    public float defaultAttackRate;
    public float defaultDestructTimer;
}
