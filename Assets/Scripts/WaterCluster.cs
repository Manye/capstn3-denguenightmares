using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterCluster : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<Camera>())
        {
            if (transform.childCount > 0)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    var child = transform.GetChild(i);
                    child.gameObject.SetActive(true);
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.GetComponent<Camera>())
        {
            if (transform.childCount > 0)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    var child = transform.GetChild(i);
                    child.gameObject.SetActive(false);
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    public void CheckRemainingWater() 
    {
        Debug.Log("Count " + this.transform.childCount);
        if (this.transform.childCount <= 2) Destroy(this.gameObject); // Hard coded because why not
    }
}
